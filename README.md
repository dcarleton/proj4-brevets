Name: Delaney Carleton
Email: dcarlet2@uoregon.edu
Repo: https://bitbucket.org/dcarleton/proj4-brevets

# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.


## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

The implementation is done in times as the input fields are filled using Ajax and Flask. Currently the miles to km (and vice versa) is implemented with Ajax. The functionality is extended as follows:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* The code for acp_times.py is implemented based on the algorithm given above.

## Testing

The testing for this code can be found in the test py file as it holds 5 test cases and can be run using nosetest.
