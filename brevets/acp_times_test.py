from acp_times import *
import arrow
import os
import nose 

def test_open_time():
    open_t = arrow.utcnow()
    assert open_time(200, 200, open_t) == open_t.shift(hours=(200/15)).isoformat()

def test_close_time():
    close_t = arrow.utcnow()
    assert close_time(200, 400, close_t) == close_t.shift(hours=(200/15)).isoformat()

def test_close_time_500():
    close_t = arrow.utcnow()
    min_time = close_t.shift(minutes = 25)
    assert close_time(600, 600, close_t) == close_t.shift(hours = (600/10)).isformat()

def test_start()
    close_t = arrow.utcnow()
    assert close_time(0, 500, close_t) == close_t.shift(hours = 1).isoformat()

def test_close_time_200():
    close_t = arrow.utcnow()
    min_time = close_t.shift(minutes = 10)
    assert close_time(200, 200, close_t) == min_time.shift(hours = (200/15)).isoformat()


